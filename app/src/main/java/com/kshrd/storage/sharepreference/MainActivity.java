package com.kshrd.storage.sharepreference;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.gson.Gson;
import com.kshrd.storage.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences(
                Constant.USER_PREFERENCE, MODE_PRIVATE
        );

        final SharedPreferences.Editor editor = sharedPreferences.edit();

        findViewById(R.id.btnPutValue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user1 = new User(100, "Annonymous");
                User user2 = new User(101, "Hacker");
                List<User> userList = new ArrayList<User>();
                userList.add(user1);
                userList.add(user2);

                String userToJson = new Gson().toJson(user1);
                String userListToJson = new Gson().toJson(userList);

                editor.putInt(Constant.USER_ID, 1);
                editor.putString(Constant.USER_NAME, "John");
                editor.putString(Constant.USER, userToJson);
                editor.putString(Constant.USER_LIST, userListToJson);

                editor.apply();

            }
        });

        findViewById(R.id.btnShowActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ShowActivity.class));
            }
        });



    }
}

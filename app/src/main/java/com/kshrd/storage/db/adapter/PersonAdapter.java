package com.kshrd.storage.db.adapter;

import android.content.Context;
import android.support.v7.widget.ListPopupWindow;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.storage.R;
import com.kshrd.storage.db.entity.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pirang on 6/8/17.
 */

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.PersonViewHolder>{

    List<Person> personList;
    MyClickListener myClickListener;

    public PersonAdapter(Context context) {
        myClickListener = (MyClickListener) context;
        this.personList = new ArrayList<>();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_layout, parent, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        Person p = personList.get(position);
        holder.tvLabel.setText(p.getName());
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public void addPerson(List<Person> list){
        this.personList.addAll(list);
    }

    public Person getPerson(int index){
        return this.personList.get(index);
    }

    public void removePerson(int pos){
        this.personList.remove(pos);
        notifyItemRemoved(pos);
    }

    public void clear(){
        this.personList.clear();
        notifyDataSetChanged();
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvLabel;
        ImageView ivDelete;

        public PersonViewHolder(View itemView) {
            super(itemView);
            tvLabel = (TextView) itemView.findViewById(R.id.tvLabel);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);

            ivDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onDeleteClicked(getAdapterPosition());
        }
    }


}

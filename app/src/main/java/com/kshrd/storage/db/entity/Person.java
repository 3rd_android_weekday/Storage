package com.kshrd.storage.db.entity;

import android.database.Cursor;

import com.kshrd.storage.db.DbConstant;

/**
 * Created by pirang on 6/7/17.
 */

public class Person {

    private int id;
    private String name;
    private String gender;

    public Person(int id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public static Person getPerson(Cursor cursor){

        if (cursor != null){
            int id = cursor.getInt(cursor.getColumnIndex(DbConstant.PERSON_ID));
            String name = cursor.getString(cursor.getColumnIndex(DbConstant.PERSON_NAME));
            String gender = cursor.getString(cursor.getColumnIndex(DbConstant.PERSON_GENDER));
            return new Person(id, name, gender);
        }
        return null;
    }
}

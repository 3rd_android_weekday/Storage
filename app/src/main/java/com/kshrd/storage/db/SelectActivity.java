package com.kshrd.storage.db;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.storage.R;
import com.kshrd.storage.db.adapter.MyClickListener;
import com.kshrd.storage.db.adapter.PersonAdapter;
import com.kshrd.storage.db.data.PersonDbHelper;
import com.kshrd.storage.db.entity.Person;

import java.util.List;

public class SelectActivity extends AppCompatActivity implements MyClickListener, TextWatcher{

    List<Person> personList;
    private PersonDbHelper personDbHelper;
    private PersonAdapter adapter;

    EditText etSearchByName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);

        etSearchByName = (EditText) findViewById(R.id.etSearchByName);
        etSearchByName.addTextChangedListener(this);

        personDbHelper = new PersonDbHelper(this);
        personList = personDbHelper.findAllPersons();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvPerson);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new PersonAdapter(this);
        adapter.addPerson(personList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDeleteClicked(int pos) {

        int id = adapter.getPerson(pos).getId();

        if (personDbHelper.deletePerson(id)){
            adapter.removePerson(pos);
            Toast.makeText(this, "Successful...!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Failed....!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        List<Person> list = personDbHelper.findPersonByName(String.valueOf(s));
        adapter.clear();
        adapter.addPerson(list);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

package com.kshrd.storage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadFileActivity extends AppCompatActivity {

    FileInputStream fis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_file);

        TextView tvLabel = (TextView) findViewById(R.id.tvLabel);


        int c;
        String temp = "";
        try {
            fis = openFileInput(FileConstant.FILE_NAME);
            while ((c = fis.read()) != -1){
                char character = (char) c;
                temp += Character.toString(character);
            }

        } catch (Exception e) {
            e.printStackTrace();
            temp += "Error";
        } finally {
            tvLabel.setText(temp);
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
